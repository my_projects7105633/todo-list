from django.db import models
from django.contrib.auth.models import User


class Task(models.Model):
    
    class Status(models.TextChoices):
        DONE = 'Done'
        AWAITING = 'Awaiting'

    title = models.CharField(max_length=40)
    author = models.ForeignKey(User,
                               on_delete=models.CASCADE,
                               related_name='tasks')
    created = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=8,
                              choices=Status.choices,
                              default=Status.AWAITING)


    class Meta:
        ordering = ['created']
        indexes = [
            models.Index(fields=['-created'])
        ]

    def __str__(self):
        return self.title
