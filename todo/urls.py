from django.urls import path
from . import views


app_name = 'todo'
urlpatterns = [
    path('', views.view_list, name='main'),
    path('add-task/', views.save_task, name='add_task'),
    path('<int:pk>/', views.remove_task, name='remove_task'),
    path('change-status/<int:pk>/',
         views.change_status, name='change_status'),
    # Operations for all tasks
    path('remove-all-tasks/',
         views.remove_all_tasks, name='remove_all_tasks'),
    path('activate-all-tasks/',
         views.activate_all_tasks, name='activate_all_tasks'),
    path('deactivate-all-tasks/',
         views.deactivate_all_tasks, name='deactivate_all_tasks'),
]
