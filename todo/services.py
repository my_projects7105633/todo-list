from typing import NamedTuple
from django.contrib.auth.models import User
from django.http import HttpRequest
from django.contrib import messages

from .forms import CreateTaskForm
from .models import Task


class TaskStatuses(NamedTuple):
    done = Task.Status.DONE
    awaiting = Task.Status.AWAITING


# view_list
def get_datas_for_main_page(request: HttpRequest) -> dict:
    author = User.objects.get(username=request.user)
    return {'task_list': author.tasks.all(),
            'form': CreateTaskForm()}


# save_task
def save_new_users_task(request: HttpRequest) -> None:
    task_form = CreateTaskForm(request.POST)
    author = User.objects.get(username=request.user)
    if task_form.is_valid:
        task = task_form.save(commit=False)
        if _check_task_title(task.title, author):
            task.author = author
            task.save()
            messages.success(request,
                             "New task added successfully")
        else:
            messages.error(request,
                          f'Task "{task.title}" is already exists')
    else:
        messages.error(request, 'Error! Form is not valide')


def _check_task_title(title: str, author: User) -> bool:
    print(f"start checking. {author=}, {title=}")
    return False if author.tasks.filter(title=title).exists() else True


# remove_task
def remove_one_users_task(request: HttpRequest, pk: int) -> None:
    author = User.objects.get(username=request.user)
    try:
        task = author.tasks.get(id=pk)
        task.delete()
        messages.success(request, f"Task {task.title} was delayed")
    except Exception as er:
        messages.error(request, f"{er}")


# change_status
def change_one_tasks_status(request: HttpRequest, pk: int) -> None:
    author = User.objects.get(username=request.user)
    try:
        task = author.tasks.get(id=pk)
        if task.status == TaskStatuses.awaiting:
            task.status = TaskStatuses.done
            task.save(update_fields=['status'])
        else:
            task.status = TaskStatuses.awaiting
        task.save(update_fields=['status'])
    except Exception as er:
        messages.error(request, f'{er}')


# remove_all_tasks
def remove_all_users_tasks(request: HttpRequest) -> None:
    try:
        user = User.objects.get(username=request.user)
        user.tasks.all().delete()
        messages.success(request, "All tasks was delayed")
    except Exception as er:
        messages.error(request, f"{er}")


# deactivate_all_tasks
def change_status_all_users_tasks(request: HttpRequest, status: str) -> None:
    try:
        user = User.objects.get(username=request.user)
        user.tasks.all().update(status=status)
        messages.success(request,
                        f"All tasks are {status}")
    except Exception as er:
        messages.error(request,
                       f"{er}")
