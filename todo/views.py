from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST, require_http_methods
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect

from .services import change_one_tasks_status, change_status_all_users_tasks, \
        get_datas_for_main_page, remove_all_users_tasks, \
        remove_one_users_task, save_new_users_task
from .services import TaskStatuses


@login_required
def view_list(request: HttpRequest) -> HttpResponse | HttpResponseRedirect:
    return render(request,
                  'todo/main.html',
                  get_datas_for_main_page(request))


@login_required
@require_POST
def save_task(request: HttpRequest) -> HttpResponse | HttpResponseRedirect:
    try:
        save_new_users_task(request)
    except Exception as er:
        return render(request,
                      'page_error.html',
                      {'error': er})
    else:
        return redirect('todo:main')


@login_required
def remove_task(request: HttpRequest,
                pk: int) -> HttpResponse | HttpResponseRedirect:
    try:
        remove_one_users_task(request, pk)
    except Exception as er:
        return render(request,
                      'page_error.html',
                      {'error': er})
    else:
        return redirect('todo:main')


@login_required
def change_status(request: HttpRequest,
                  pk: int) -> HttpResponse | HttpResponseRedirect:
    try:
        change_one_tasks_status(request, pk)
    except Exception as er:
        return render(request,
                      'page_error.html',
                      {'error': er})
    else:
        return redirect('todo:main')


@login_required
@require_POST
def remove_all_tasks(request: HttpRequest
                     ) -> HttpResponse | HttpResponseRedirect:
    try:
        remove_all_users_tasks(request)
    except Exception as er:
        return render(request,
                      'page_error.html',
                      {'error': er})
    else:
        return redirect('todo:main')


@login_required
@require_POST
def deactivate_all_tasks(request: HttpRequest
                         ) -> HttpResponse | HttpResponseRedirect:
    try:
        change_status_all_users_tasks(request, TaskStatuses.done)
    except Exception as er:
        return render(request,
                      'page_error.html',
                      {'error': er})
    else:
        return redirect('todo:main')


@login_required
@require_POST
def activate_all_tasks(request: HttpRequest
                       ) -> HttpResponse | HttpResponseRedirect:
    try:
        change_status_all_users_tasks(request, TaskStatuses.awaiting)
    except Exception as er:
        return render(request,
                      'page_error.html',
                      {'error': er})
    else:
        return redirect('todo:main')
