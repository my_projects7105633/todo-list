from django.shortcuts import render, redirect
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect

from .services import registrator
from .forms import CreateUserForm


def registration(request: HttpRequest) -> HttpResponse | HttpResponseRedirect:
    form = registrator(request)
    if form:
        return render(request,
                    'register.html',
                    {'form': CreateUserForm()})
    else:
        return redirect('/')


