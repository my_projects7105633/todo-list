from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class CreateUserForm(UserCreationForm):
    #password = forms.CharField(label='Password',
    #                           widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username']

#    def clean_password2(self):
#        cd = self.cleaned_data
#        if cd['password'] != cd['password2']:
#            raise forms.ValidationError("Passwords aren't same")
#        return cd['password2']
