from django.urls import path, include

from .views import registration


urlpatterns = [
    # Logining
    path('', include('django.contrib.auth.urls')),
    # Registration
    path('register/', registration, name='register'),
]
