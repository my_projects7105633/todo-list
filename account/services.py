
from .forms import CreateUserForm

def registrator(request):
    if request.method == 'POST':
        registr_form = CreateUserForm(request.POST)
        if registr_form.is_valid():
            registr_form.save()
            return None
        else:
            return CreateUserForm()
    else:
        return CreateUserForm()
